import sys, array
import Image

infile = sys.argv[1]
im = Image.open(infile)

outfile = infile.split('.')[0] + ".bin"
fout = open(outfile, "wb")

#Requires a 24-bit input image, like a nice 24-bit png
binary = array.array('B')
for pixel in im.getdata():
	blue = pixel[0]
	red = pixel[1]
	green = pixel[2]
	alpha = 0
	#vesafb doesn't use rgba ordering- it's brga
	for i in (blue, red, green, alpha):
		binary.append(i)

binary.tofile(fout)
fout.close()