/* fb-hitler: the hitlerinator
 * Copyright (C) 2009 Peter Marheine
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *  
  * This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <sys/mount.h>
#include <linux/fb.h>

//640x480 at 24 bpp
#define IMGSIZE 680*480*4
#define SLEEP_USECS 500000
#define FBDEV "/dev/fb0"

void *loadImg(char *filename);
void redrawFB(void *fb, void *image);
void catchCAD();

//catchCAD uses these to toggle the image
volatile void *currentHitler;
void *hitler, *hitler2;

int main(int argc, char *argv[])
{
	int fd;
	void *fbmap;
	struct fb_var_screeninfo screeninfo;

	/* Create a device node for the framebuffer (we don't have udev to do it for us)
	  * Character device: major 29, minor 0
	   * As for mounting /dev, should make it a ramfs (don't want to install files in the
	   * user's /dev, now do we?) */
	if ((mount("none","/dev","ramfs",NULL," ")) == -1)
	{
		printf("Failure mounting /dev\n");
		exit(1);
	}
	if ((mknod(FBDEV,S_IFCHR | S_IRWXU,makedev(29,0))) == -1)
	{
		printf("Unable to create device node for %s\n",FBDEV);
		exit(1);
	}
	//Open a framebuffer
	fd = open(FBDEV, O_RDWR);
	if (fd == -1)
	{
		printf("Unable to open %s\n",FBDEV);
		goto cleanupFB;
	}

	//Setup framebuffer
	ioctl(fd, FBIOGET_VSCREENINFO, &screeninfo);
	//Set if to 640x480-32
	screeninfo.xres = 640;
	screeninfo.yres = 480;
	screeninfo.bits_per_pixel = 32;
	if ((ioctl(fd, FBIOPUT_VSCREENINFO, &screeninfo)) == -1)
	{
		printf("Error setting framebuffer to 640x480-32\n");
		goto cleanupFB;
	}
	//mmap the framebuffer
	fbmap = mmap(NULL, IMGSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (fbmap == (char *)-1)
	{
		printf("Unable to mmap file\n");
		goto cleanupFB;
	}
	
	//Load images
	hitler = loadImg("/hitler.bin");
	hitler2 = loadImg("/hitler2.bin");
	if (!hitler || !hitler2)
	{
		printf("Couldn't load one or both image(s)\n");
		goto cleanupLoad;
	}
	
	//Set up CAD-catching signal handler
	if ((reboot(RB_DISABLE_CAD)) == -1)
	{
		printf("CAD disable failed!\n");
		goto cleanupLoad;
	}
	signal(SIGINT, catchCAD);
	
	//Hide framebuffer's cursor
	//This works on my normal terminal, but not when this is init?
	printf("\033[?1c");
	//Set up image pointers, then infinite loop
	currentHitler = hitler;
	redrawFB(fbmap, currentHitler);
	while (1)
	{
		if (currentHitler != hitler)
		{
			redrawFB(fbmap, currentHitler);
			usleep(SLEEP_USECS);
			currentHitler = hitler;
			redrawFB(fbmap, currentHitler);
		}
	}
cleanupLoad:
	free(hitler);
	free(hitler2);
	munmap(fbmap,IMGSIZE);
cleanupFB:
	close(fd);
	exit(1);
}

void catchCAD()
{
	currentHitler = hitler2;
}

void redrawFB(void *fb, void *image)
{
	memcpy(fb, image, IMGSIZE);
}

void *loadImg(char *filename)
{
	FILE *fp;
	char *file;
	fp = fopen(filename, "rb");
	if (!fp)
	{
		return(NULL);
	}
	file = malloc(IMGSIZE);
	if (!file)
	{
		return(NULL);
	}
	fread(file, 1, IMGSIZE, fp);
	fclose(fp);
	return(file);
}